package io.freetlgrm.beststickers

import android.os.Bundle
import android.text.TextUtils
import io.freetlgrm.beststickers.adapters.ListAdapter
import io.freetlgrm.beststickers.adapters.ListDecoration
import io.freetlgrm.beststickers.adapters.ListListener
import io.freetlgrm.beststickers.base.BaseV4Fragment
import io.freetlgrm.beststickers.models.StickerPack
import kotlinx.android.synthetic.main.fragment_list.*

@Suppress("MemberVisibilityCanBePrivate")
class ListFragment : BaseV4Fragment() {

    override val layout: Int = R.layout.fragment_list

    val mainActivity: MainActivity? get() = activity as MainActivity?

    var adapter = ListAdapter(object : ListListener {

        override fun onSelect(stickerPack: StickerPack) {
            mainActivity?.onSelectSticker(stickerPack)
        }
    })

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recycler_view.setHasFixedSize(true)
        recycler_view.adapter = adapter
        recycler_view.addItemDecoration(ListDecoration())
        adapter.items.clear()
        onNewStickers()
    }

    fun onNewStickers() {
        if (view == null) {
            return
        }
        val categoryUid = args().getString("category")
        if (categoryUid == null) {
            onSearch()
            return
        }
        mainActivity?.stickersPack?.forEach {
            if (it.category == categoryUid) {
                var i = 0
                while (i < adapter.items.size) {
                    if (adapter.items[i].uid == it.uid) {
                        i = -1
                        break
                    }
                    i++
                }
                if (i >= 0) {
                    adapter.items.add(it)
                }
            }
        }
        adapter.notifyDataSetChanged()
    }

    fun onSearch() {
        if (view == null) {
            return
        }
        val categoryUid = args().getString("category")
        if (categoryUid != null) {
            return
        }
        adapter.items.clear()
        mainActivity?.let { activity ->
            activity.stickersPack.forEach {
                if (TextUtils.isEmpty(activity.query) || it.matches(activity.query)) {
                    adapter.items.add(it)
                }
            }
        }
        adapter.notifyDataSetChanged()
    }

    companion object {

        fun newInstance(uid: String?): ListFragment {
            val fragment = ListFragment()
            fragment.arguments = Bundle().apply {
                putString("category", uid)
            }
            return fragment
        }
    }
}