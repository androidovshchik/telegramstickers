package io.freetlgrm.beststickers.adapters

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.jetbrains.anko.dip

class ListDecoration : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = parent.getChildLayoutPosition(view)
        val space = view.context.dip(8)
        if (position % 2 == 0) {
            outRect.left = space
        } else {
            outRect.left = 0
        }
        outRect.right = space
        if (position < 2) {
            outRect.top = space
        } else {
            outRect.top = 0
        }
        outRect.bottom = space
    }
}