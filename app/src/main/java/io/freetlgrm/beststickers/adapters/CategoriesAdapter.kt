package io.freetlgrm.beststickers.adapters

import android.util.SparseArray
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import io.freetlgrm.beststickers.ListFragment

class CategoriesAdapter(manager: FragmentManager) : FragmentStatePagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val fragments = SparseArray<ListFragment>()

    private val uids = ArrayList<String?>()

    private val titles = ArrayList<String>()

    override fun getItem(position: Int): ListFragment {
        return fragments.get(position, ListFragment.newInstance(uids[position]).apply {
            fragments.put(position, this)
        })
    }

    override fun getCount(): Int = uids.size

    override fun getPageTitle(position: Int): CharSequence? = titles[position]

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        super.destroyItem(container, position, `object`)
        fragments.remove(position)
    }

    fun addFragment(uid: String?, title: String) {
        uids.add(uid)
        titles.add(title)
    }
}