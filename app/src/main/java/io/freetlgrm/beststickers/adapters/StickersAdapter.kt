package io.freetlgrm.beststickers.adapters

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import io.freetlgrm.beststickers.R
import io.freetlgrm.beststickers.models.Sticker
import io.freetlgrm.beststickers.stickers.GlideApp
import java.util.*

class StickersAdapter(var listener: StickersListener, val height: Int) : RecyclerView.Adapter<StickersAdapter.ViewHolder>() {

    var items: ArrayList<Sticker> = ArrayList()

    var isAnimated = false

    private val placeholder = ColorDrawable(Color.WHITE)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_sticker,
            parent, false), viewType)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        GlideApp.with(holder.context)
            .load(item)
            .placeholder(placeholder)
            .error(placeholder)
            .into(holder.preview)
    }

    override fun onViewRecycled(holder: ViewHolder) {
        GlideApp.with(holder.context)
            .clear(holder.preview)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    @Suppress("UNUSED_PARAMETER")
    inner class ViewHolder(itemView: View, position: Int) : RecyclerView.ViewHolder(itemView) {

        var preview: ImageView = itemView.findViewById(R.id.preview)

        init {
            preview.layoutParams.height = height
        }

        val context: Context get() = itemView.context.applicationContext
    }
}
