package io.freetlgrm.beststickers.adapters

import io.freetlgrm.beststickers.models.StickerPack

interface ListListener {

    fun onSelect(stickerPack: StickerPack)
}