package io.freetlgrm.beststickers.adapters

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.jetbrains.anko.dip

class StickersDecoration : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val maxSpace = view.context.dip(16)
        val minSpace = view.context.dip(8)
        val position = parent.getChildAdapterPosition(view)
        val linesCount = state.itemCount / 4 + if (state.itemCount % 4 != 0) 1 else 0
        val minBottomItemPosition = (linesCount - 1) * 4
        if (position <= 3) {
            outRect.top = maxSpace
        } else {
            outRect.top = minSpace
        }
        if (position >= minBottomItemPosition) {
            outRect.bottom = maxSpace
        } else {
            outRect.bottom = 0
        }
        outRect.left = minSpace
        outRect.right = 0
    }
}