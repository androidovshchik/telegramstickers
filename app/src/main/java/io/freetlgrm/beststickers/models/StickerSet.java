
package io.freetlgrm.beststickers.models;

import androidx.annotation.Keep;

import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.PropertyName;

import java.io.Serializable;
import java.util.List;

@Keep
@IgnoreExtraProperties
public class StickerSet implements Serializable {

    @PropertyName("name")
    public String name;
    @PropertyName("is_animated")
    public boolean isAnimated = false;
    @PropertyName("stickers")
    public List<Sticker> stickers;

    @Override
    public String toString() {
        return "StickerSet{" +
            "name='" + name + '\'' +
            ", isAnimated=" + isAnimated +
            ", stickers=" + stickers +
            '}';
    }
}
