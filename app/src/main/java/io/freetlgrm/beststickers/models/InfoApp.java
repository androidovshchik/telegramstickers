package io.freetlgrm.beststickers.models;

import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.Keep;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.PropertyName;

import static io.freetlgrm.beststickers.ConstantsKt.PREFERENCE_BOT_TOKEN;
import static io.freetlgrm.beststickers.ConstantsKt.PREFERENCE_PROXY_HOST;
import static io.freetlgrm.beststickers.ConstantsKt.PREFERENCE_PROXY_PASSWORD;
import static io.freetlgrm.beststickers.ConstantsKt.PREFERENCE_PROXY_PORT;
import static io.freetlgrm.beststickers.ConstantsKt.PREFERENCE_PROXY_USERNAME;

@Keep
@IgnoreExtraProperties
public class InfoApp {

    @PropertyName("botToken")
    public String botToken;
    @PropertyName("proxyHost")
    public String proxyHost;
    @PropertyName("proxyPort")
    public String proxyPort;
    @PropertyName("proxyUsername")
    public String proxyUsername;
    @PropertyName("proxyPassword")
    public String proxyPassword;

    @Exclude
    public boolean isValid() {
        return !TextUtils.isEmpty(botToken) && !TextUtils.isEmpty(proxyHost) && getPort() > 0 &&
            !TextUtils.isEmpty(proxyPassword) && !TextUtils.isEmpty(proxyUsername);
    }

    @Exclude
    public int getPort() {
        try {
            return Integer.parseInt(proxyPort);
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * @return true if it's needed to refresh client with new data
     */
    public boolean loadData(SharedPreferences preferences) {
        String _botToken = botToken, _proxyHost = proxyHost, _proxyPort = proxyPort,
            _proxyUsername = proxyUsername, _proxyPassword = proxyPassword;
        botToken = preferences.getString(PREFERENCE_BOT_TOKEN, null);
        proxyHost = preferences.getString(PREFERENCE_PROXY_HOST, null);
        proxyPort = preferences.getString(PREFERENCE_PROXY_PORT, null);
        proxyUsername = preferences.getString(PREFERENCE_PROXY_USERNAME, null);
        proxyPassword = preferences.getString(PREFERENCE_PROXY_PASSWORD, null);
        return !("" + _botToken).equals(botToken) || !("" + _proxyHost).equals(proxyHost) ||
            !("" + _proxyPort).equals(proxyPort) || !("" + _proxyUsername).equals(proxyUsername) ||
            !("" + _proxyPassword).equals(proxyPassword);
    }

    @Override
    public String toString() {
        return "InfoApp{" +
            "botToken='" + botToken + '\'' +
            ", proxyHost='" + proxyHost + '\'' +
            ", proxyPassword='" + proxyPassword + '\'' +
            ", proxyPort='" + proxyPort + '\'' +
            ", proxyUsername='" + proxyUsername + '\'' +
            '}';
    }
}
