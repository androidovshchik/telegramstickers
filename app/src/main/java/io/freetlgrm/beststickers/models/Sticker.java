
package io.freetlgrm.beststickers.models;

import androidx.annotation.Keep;

import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.PropertyName;

import java.io.Serializable;

@Keep
@IgnoreExtraProperties
public class Sticker implements Serializable {

    @PropertyName("emoji")
    public String emoji;
    @PropertyName("file_id")
    public String fileId;
    @PropertyName("is_animated")
    public boolean isAnimated = false;
    @PropertyName("set_name")
    public String name;

    @Override
    public String toString() {
        return "Sticker{" +
            "emoji='" + emoji + '\'' +
            ", fileId='" + fileId + '\'' +
            ", isAnimated=" + isAnimated +
            ", name='" + name + '\'' +
            '}';
    }
}
