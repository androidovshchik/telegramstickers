package io.freetlgrm.beststickers.models;

import androidx.annotation.Keep;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.PropertyName;

import java.util.Locale;

@Keep
@IgnoreExtraProperties
public class Category {

    @Exclude
    public String uid;
    @PropertyName("title")
    public String title;
    @PropertyName("titleEn")
    public String titleEn;

    @Exclude
    public String getBestTitle() {
        return Locale.getDefault().getLanguage().equalsIgnoreCase("ru")
            || titleEn == null ? title : titleEn;
    }

    @Override
    public String toString() {
        return "Category{" +
            "uid='" + uid + '\'' +
            ", title='" + title + '\'' +
            ", titleEn='" + titleEn + '\'' +
            '}';
    }
}
