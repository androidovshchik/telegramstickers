
package io.freetlgrm.beststickers.models;

import androidx.annotation.Keep;
import androidx.annotation.Nullable;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.PropertyName;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

import timber.log.Timber;

@Keep
@IgnoreExtraProperties
public class StickerPack implements Cloneable, Serializable {

    @Exclude
    public String uid;
    @PropertyName("category")
    public String category;
    @PropertyName("keywords")
    public List<String> keywords;
    @PropertyName("preview")
    public String preview;
    @Nullable
    @PropertyName("stickerSet")
    public StickerSet stickerSet;
    @PropertyName("title")
    public String title;
    @PropertyName("titleEn")
    public String titleEn;

    @Exclude
    public String getBestTitle() {
        return Locale.getDefault().getLanguage().equalsIgnoreCase("ru")
            || titleEn == null ? title : titleEn;
    }

    @Exclude
    @Nullable
    public Sticker getPreviewSticker() {
        if (stickerSet == null) {
            return null;
        }
        for (int s = 0; s < stickerSet.stickers.size(); s++) {
            if (stickerSet.stickers.get(s).fileId.equals(preview)) {
                return stickerSet.stickers.get(s);
            }
        }
        return null;
    }

    @Exclude
    public boolean matches(String query) {
        for (int k = 0; k < keywords.size(); k++) {
            if (keywords.get(k).toLowerCase().contains(query)) {
                return true;
            }
        }
        return false;
    }

    @Exclude
    @Override
    public StickerPack clone() {
        try {
            return (StickerPack) super.clone();
        } catch (CloneNotSupportedException e) {
            Timber.e(e);
            return null;
        }
    }

    @Override
    public String toString() {
        return "StickerPack{" +
            "uid='" + uid + '\'' +
            ", category='" + category + '\'' +
            ", keywords=" + keywords +
            ", preview='" + preview + '\'' +
            ", stickerSet=" + stickerSet +
            ", title='" + title + '\'' +
            ", titleEn='" + titleEn + '\'' +
            '}';
    }
}
