
package io.freetlgrm.beststickers.models;

import androidx.annotation.Keep;

import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.PropertyName;

import java.io.Serializable;

@Keep
@IgnoreExtraProperties
public class Thumb implements Serializable {

    @PropertyName("file_id")
    public String fileId;

    @Override
    public String toString() {
        return "Thumb{" +
            "fileId='" + fileId + '\'' +
            '}';
    }
}
