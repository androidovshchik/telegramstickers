
package io.freetlgrm.beststickers.models;

import android.annotation.SuppressLint;

import androidx.annotation.Keep;

import com.google.firebase.firestore.IgnoreExtraProperties;
import com.google.firebase.firestore.PropertyName;

import java.io.Serializable;
import java.util.Calendar;

@Keep
@IgnoreExtraProperties
public class Feedback implements Serializable {

    @PropertyName("text")
    public String text;

    @PropertyName("time")
    public String time;

    public Feedback() {
    }

    @SuppressLint("DefaultLocale")
    public Feedback(String text) {
        this.text = text;
        time = String.format("%1$tY-%<tm-%<td %<tR", Calendar.getInstance());
    }

    @Override
    public String toString() {
        return "Feedback{" +
            "text='" + text + '\'' +
            '}';
    }
}
