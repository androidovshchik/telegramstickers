package io.freetlgrm.beststickers

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.codemybrainsout.ratingdialog.RatingDialog
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.yandex.metrica.YandexMetrica
import durdinapps.rxfirebase2.RxFirestore
import io.freetlgrm.beststickers.adapters.CategoriesAdapter
import io.freetlgrm.beststickers.base.BaseV7Activity
import io.freetlgrm.beststickers.extensions.sharedPreferences
import io.freetlgrm.beststickers.models.Category
import io.freetlgrm.beststickers.models.Feedback
import io.freetlgrm.beststickers.models.InfoApp
import io.freetlgrm.beststickers.models.StickerPack
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber

class MainActivity : BaseV7Activity(), ViewPager.OnPageChangeListener {

    private lateinit var adapter: CategoriesAdapter

    private lateinit var interstitialAd: InterstitialAd

    private var touchEvents = 0

    private var loadingAds = false

    var stickersPack = ArrayList<StickerPack>()

    var query = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        search.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                if (pager.currentItem != 0) {
                    pager.setCurrentItem(0, true)
                }
                query = s.toString().trim().toLowerCase()
                synchronized(this@MainActivity) {
                    adapter.fragments.forEach {
                        it.onSearch()
                    }
                }
            }
        })
        adapter = CategoriesAdapter(supportFragmentManager)
        adapter.addFragment(ListFragment.newInstance(null), getString(R.string.all_stickers))
        pager.offscreenPageLimit = 3
        pager.adapter = adapter
        pager.addOnPageChangeListener(this)
        tabs.setupWithViewPager(pager)
        disposable.add(RxFirestore.getCollection(FirebaseFirestore.getInstance()
            .collection("categories")
            .orderBy("title", Query.Direction.ASCENDING))
            .subscribe({
                synchronized(this) {
                    it.documents.forEach { document ->
                        document.toObject(Category::class.java)?.let { category ->
                            category.uid = document.id
                            adapter.addFragment(ListFragment.newInstance(category.uid), category.bestTitle)
                        }
                    }
                    adapter.fragments.forEach { fragment ->
                        fragment.onNewStickers()
                    }
                }
                adapter.notifyDataSetChanged()
            }, {}))
        disposable.add(RxFirestore.getDocument(FirebaseFirestore.getInstance()
            .collection("info")
            .document("app"))
            .subscribe({
                val preferences = sharedPreferences
                it.toObject(InfoApp::class.java)?.let { infoApp ->
                    preferences.edit().apply {
                        putString(PREFERENCE_BOT_TOKEN, infoApp.botToken)
                        putString(PREFERENCE_PROXY_HOST, infoApp.proxyHost)
                        putString(PREFERENCE_PROXY_PORT, infoApp.proxyPort)
                        putString(PREFERENCE_PROXY_USERNAME, infoApp.proxyUsername)
                        putString(PREFERENCE_PROXY_PASSWORD, infoApp.proxyPassword)
                        apply()
                    }
                }
                if (MainApplication.infoApp.loadData(preferences)) {
                    (application as MainApplication).buildClient()
                }
                loadStickers(null)
            }, {}))
        interstitialAd = InterstitialAd(applicationContext)
        interstitialAd.adUnitId = getString(R.string.ads_between)
        interstitialAd.adListener = object : AdListener() {

            override fun onAdLoaded() {
                Timber.d("onAdLoaded")
                interstitialAd.show()
            }

            override fun onAdFailedToLoad(errorCode: Int) {
                Timber.e("onAdFailedToLoad errorCode=$errorCode")
                loadingAds = false
            }

            override fun onAdOpened() {
                Timber.d("onAdOpened")
            }

            override fun onAdLeftApplication() {
                Timber.d("onAdLeftApplication")
            }

            override fun onAdClosed() {
                Timber.d("onAdClosed")
                loadingAds = false
            }
        }
        /*logs.setOnClickListener {
            Toast.makeText(applicationContext, "Подождите...",
                Toast.LENGTH_SHORT).show()
            disposable.add(Observable.fromCallable {
                val appLogs = try {
                    val process = Runtime.getRuntime().exec("logcat -d")
                    process.inputStream.bufferedReader().use(BufferedReader::readText)
                } catch (e: Exception) {
                    Timber.e(e)
                    e.toString()
                }
                appLogs
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { appLogs: String ->
                    sendEmail(appLogs)
                })
        }*/
        RatingDialog.Builder(this)
            .icon(ContextCompat.getDrawable(applicationContext, R.drawable.rate))
            .threshold(4f)
            .session(3)
            .onRatingBarFormSumbit {
                disposable.add(RxFirestore.setDocument(FirebaseFirestore.getInstance()
                    .collection("feedback")
                    .document("${System.currentTimeMillis()}-${(0..999).random()}"), Feedback(it))
                    .subscribe())
            }
            .build()
            .show()
    }

    override fun onStart() {
        super.onStart()
        layout.requestFocus()
    }

    /*private fun sendEmail(logs: String) {
        if (TextUtils.isEmpty(logs)) {
            Toast.makeText(applicationContext, "Не удалось получить логи",
                Toast.LENGTH_SHORT).show()
            return
        }
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf("viktorpavlov11111@gmail.com"))
        intent.putExtra(Intent.EXTRA_SUBJECT, "Логи")
        intent.putExtra(Intent.EXTRA_TEXT, logs)
        try {
            startActivity(Intent.createChooser(intent, "Отправить письмо..."))
        } catch (e: Exception) {
            Timber.e(e)
            Toast.makeText(applicationContext, "Не удалось отправить письмо",
                Toast.LENGTH_SHORT).show()
        }
    }*/

    private fun loadStickers(snapshot: DocumentSnapshot?) {
         val collectionQuery = if (snapshot != null) {
             FirebaseFirestore.getInstance()
                 .collection("stickers-pack")
                 .orderBy("title", Query.Direction.ASCENDING)
                 .startAfter(snapshot)
                 .limit(20)
         } else {
             FirebaseFirestore.getInstance()
                 .collection("stickers-pack")
                 .orderBy("title", Query.Direction.ASCENDING)
                 .limit(20)
         }
        disposable.add(RxFirestore.getCollection(collectionQuery)
            .subscribe({
                if (it.documents.size > 0) {
                    synchronized(this) {
                        it.documents.forEach { document ->
                            document.toObject(StickerPack::class.java)?.let { stickerPack ->
                                if (stickerPack.stickerSet != null) {
                                    stickerPack.uid = document.id
                                    stickerPack.stickerSet!!.stickers.forEach { sticker ->
                                        sticker.name = stickerPack.stickerSet!!.name
                                    }
                                    stickersPack.add(stickerPack)
                                }
                            }
                        }
                        adapter.fragments.forEach { fragment ->
                            fragment.onNewStickers()
                        }
                    }
                    loadStickers(it.documents.last())
                }
            }, {}))
    }

    fun onSelectSticker(stickerPack: StickerPack) {
        val intent = Intent(applicationContext, StickersActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra("sticker-pack", stickerPack)
        startActivity(intent)
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            touchEvents++
            if (touchEvents >= 15) {
                touchEvents = 0
                if (!loadingAds) {
                    loadingAds = true
                    interstitialAd.loadAd(AdRequest.Builder()
                        .build())
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    override fun onPageScrollStateChanged(state: Int) {}

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    override fun onPageSelected(position: Int) {
        if (!BuildConfig.DEBUG) {
            YandexMetrica.reportEvent("category_${adapter.getPageTitle(position)}")
        }
    }

    override fun onDestroy() {
        pager.removeOnPageChangeListener(this)
        super.onDestroy()
    }
}
