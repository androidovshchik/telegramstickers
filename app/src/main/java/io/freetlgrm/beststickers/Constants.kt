package io.freetlgrm.beststickers

const val PREFERENCE_PROXY_HOST = "proxyHost"
const val PREFERENCE_PROXY_PORT = "proxyPort"
const val PREFERENCE_PROXY_USERNAME = "proxyUsername"
const val PREFERENCE_PROXY_PASSWORD = "proxyPassword"
const val PREFERENCE_BOT_TOKEN = "botToken"
const val STORAGE_URL = "https://firebasestorage.googleapis.com/v0/b/stickers-telegram.appspot.com/o"
const val BACKUP_URL = "$STORAGE_URL/stickers-backup"