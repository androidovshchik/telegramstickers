package io.freetlgrm.beststickers

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.yandex.metrica.YandexMetrica
import io.freetlgrm.beststickers.adapters.StickersAdapter
import io.freetlgrm.beststickers.adapters.StickersDecoration
import io.freetlgrm.beststickers.adapters.StickersListener
import io.freetlgrm.beststickers.base.BaseV7Activity
import io.freetlgrm.beststickers.extensions.windowSize
import io.freetlgrm.beststickers.models.StickerPack
import kotlinx.android.synthetic.main.activity_stickers.*
import org.jetbrains.anko.dip

class StickersActivity : BaseV7Activity() {

    private lateinit var adapter: StickersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stickers)
        val stickerPack = intent.getSerializableExtra("sticker-pack") as StickerPack
        title = stickerPack.bestTitle
        adapter = StickersAdapter(object : StickersListener {}, (windowManager.windowSize.x - dip(56)) / 4).apply {
            isAnimated = stickerPack.stickerSet!!.isAnimated
        }
        recycler_view.setHasFixedSize(true)
        recycler_view.addItemDecoration(StickersDecoration())
        adapter.items.addAll(stickerPack.stickerSet!!.stickers)
        recycler_view.adapter = adapter
        add.setOnClickListener {
            if (!BuildConfig.DEBUG) {
                YandexMetrica.reportEvent("click_add_${stickerPack.uid}")
            }
            try {
                startActivity(Intent(Intent.ACTION_VIEW,
                    Uri.parse("tg://addstickers?set=${stickerPack.stickerSet!!.name}")))
            } catch (e: Exception) {
                try {
                    startActivity(Intent(Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=org.telegram.messenger")))
                } catch (e: Exception) {
                    startActivity(Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=org.telegram.messenger")))
                }
            }
        }
    }
}
