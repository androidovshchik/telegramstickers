package io.freetlgrm.beststickers

import android.app.Application
import android.content.Context
import android.util.Base64
import androidx.multidex.MultiDex
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.Volley
import com.google.android.gms.ads.MobileAds
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig
import io.freetlgrm.beststickers.extensions.sharedPreferences
import io.freetlgrm.beststickers.models.InfoApp
import timber.log.Timber
import java.io.IOException
import java.net.*

@Suppress("unused")
class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Class.forName("com.facebook.stetho.Stetho")
                .getDeclaredMethod("initializeWithDefaults", Context::class.java)
                .invoke(null, applicationContext)
        }
        if (infoApp.loadData(sharedPreferences)) {
            buildClient()
        }
        MobileAds.initialize(applicationContext, getString(R.string.ads_app))
        if (!BuildConfig.DEBUG) {
            YandexMetrica.activate(applicationContext, YandexMetricaConfig.newConfigBuilder(getString(R.string.yandex_api_key))
                .build())
            YandexMetrica.enableActivityAutoTracking(this)
        }
    }

    override fun attachBaseContext(context: Context) {
        super.attachBaseContext(context)
        MultiDex.install(this)
    }

    fun buildClient() {
        if (queue != null) {
            queue?.stop()
            queue = null
        }
        val host = infoApp.proxyHost
        val port = infoApp.port
        queue = Volley.newRequestQueue(applicationContext, object : HurlStack() {

            @Throws(IOException::class)
            override fun createConnection(url: URL): HttpURLConnection {
                val proxy = Proxy(Proxy.Type.HTTP, InetSocketAddress.createUnresolved(host, port))
                return url.openConnection(proxy) as HttpURLConnection
            }
        })
    }

    companion object {

        @JvmStatic
        val infoApp: InfoApp = InfoApp()

        @JvmStatic
        private var queue: RequestQueue? = null

        @JvmStatic
        fun getFilePath(fileId: String) = "https://api.telegram.org/bot${infoApp.botToken}/getFile?file_id=${URLEncoder.encode(fileId, "UTF-8")}"

        @JvmStatic
        fun downloadFile(path: String) = "https://api.telegram.org/file/bot${infoApp.botToken}/${URLEncoder.encode(path, "UTF-8")}"

        @JvmStatic
        fun getHeaders(): Map<String, String> {
            val credentials = Base64.encodeToString("${infoApp.proxyUsername}:${infoApp.proxyPassword}"
                .toByteArray(), Base64.NO_WRAP)
            val headers = HashMap<String, String>()
            headers["Authenticate"] = "Basic $credentials"
            headers["Authorization"] = "Basic $credentials"
            headers["Proxy-Authorization"] = "Basic $credentials"
            headers["Proxy-Authenticate"] = "Basic $credentials"
            headers["WWW-Authorization"] = "Basic $credentials"
            headers["WWW-Authenticate"] = "Basic $credentials"
            return headers
        }

        @JvmStatic
        fun addRequest(tag: Any, request: Request<out Any>) {
            request.tag = tag
            queue?.add(request)
        }

        @JvmStatic
        fun cancelRequests(tag: Any) {
            queue?.cancelAll(tag)
        }
    }
}