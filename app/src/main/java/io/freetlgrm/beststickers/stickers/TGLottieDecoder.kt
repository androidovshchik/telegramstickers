package io.freetlgrm.beststickers.stickers

import com.airbnb.lottie.LottieCompositionFactory
import com.airbnb.lottie.LottieDrawable
import com.bumptech.glide.load.Options
import com.bumptech.glide.load.ResourceDecoder
import com.bumptech.glide.load.engine.Resource
import timber.log.Timber
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.util.zip.GZIPInputStream

class TGLottieDecoder : ResourceDecoder<ByteArray, LottieDrawable> {

    override fun handles(source: ByteArray, options: Options): Boolean {
        return GZIPInputStream.GZIP_MAGIC == source[0].toInt() and 0xff or (source[1].toInt() shl 8 and 0xff00)
    }

    override fun decode(source: ByteArray, width: Int, height: Int, options: Options): Resource<LottieDrawable>? {
        Timber.d(options.toString())
        val json = GZIPInputStream(ByteArrayInputStream(source))
            .bufferedReader()
            .use(BufferedReader::readText)
        return LottieResource(LottieDrawable().apply {
            composition = LottieCompositionFactory.fromJsonStringSync(json, null).value
        })
    }
}
