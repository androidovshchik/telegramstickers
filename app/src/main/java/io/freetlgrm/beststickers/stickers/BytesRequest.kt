package io.freetlgrm.beststickers.stickers

import com.android.volley.AuthFailureError
import com.android.volley.NetworkResponse
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import io.freetlgrm.beststickers.MainApplication

class BytesRequest(url: String, private val successListener: Response.Listener<ByteArray>, errorListener: Response.ErrorListener)
    : Request<ByteArray>(Method.GET, url, errorListener) {

    init {
        setShouldCache(false)
    }

    @Throws(AuthFailureError::class)
    override fun getHeaders(): Map<String, String> {
        return MainApplication.getHeaders()
    }

    override fun deliverResponse(response: ByteArray) {
        successListener.onResponse(response)
    }

    override fun parseNetworkResponse(response: NetworkResponse): Response<ByteArray> {
        return Response.success(response.data, HttpHeaderParser.parseCacheHeaders(response))
    }
}