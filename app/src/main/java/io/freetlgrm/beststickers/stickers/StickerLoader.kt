package io.freetlgrm.beststickers.stickers

import com.android.volley.Response
import com.android.volley.VolleyError
import com.bumptech.glide.Priority
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.Key
import com.bumptech.glide.load.Options
import com.bumptech.glide.load.data.DataFetcher
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.ModelLoaderFactory
import com.bumptech.glide.load.model.MultiModelLoaderFactory
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StreamDownloadTask
import io.freetlgrm.beststickers.MainApplication
import io.freetlgrm.beststickers.models.Sticker
import java.security.MessageDigest

class StickerLoader : ModelLoader<Sticker, ByteArray> {
    
    @Suppress("unused")
    class Factory : ModelLoaderFactory<Sticker, ByteArray> {

        override fun build(factory: MultiModelLoaderFactory): ModelLoader<Sticker, ByteArray> {
            return StickerLoader()
        }

        override fun teardown() {}
    }

    override fun buildLoadData(reference: Sticker, height: Int, width: Int, options: Options): ModelLoader.LoadData<ByteArray>? {
        return ModelLoader.LoadData(StickerKey(reference), StickerFetcher(reference))
    }

    override fun handles(reference: Sticker): Boolean {
        return true
    }
}

private class StickerKey(private val ref: Sticker) : Key {

    override fun updateDiskCacheKey(digest: MessageDigest) {
        digest.update(ref.fileId.toByteArray(Charsets.UTF_8))
    }
}

private class StickerFetcher(private val ref: Sticker) : DataFetcher<ByteArray> {

    private var streamTask: StreamDownloadTask? = null

    override fun loadData(priority: Priority, callback: DataFetcher.DataCallback<in ByteArray>) {
        MainApplication.apply {
            if (infoApp.isValid) {
                callback.onLoadFailed(IllegalAccessException())
                return
            }
            addRequest(ref.fileId, JsonRequest(getFilePath(ref.fileId), Response.Listener { response ->
                val path = response.getJSONObject("result")
                    .getString("file_path")
                addRequest(ref.fileId, BytesRequest(downloadFile(path), Response.Listener { bytes ->
                    callback.onDataReady(bytes)
                }, Response.ErrorListener { error ->
                    onRequestError(error, callback)
                }))
            }, Response.ErrorListener { error ->
                onRequestError(error, callback)
            }))
        }
    }

    fun onRequestError(error: VolleyError, callback: DataFetcher.DataCallback<in ByteArray>) {
        if (error.message?.contains("authenticate", true) != true) {
            callback.onLoadFailed(error)
            return
        }
        streamTask = FirebaseStorage.getInstance()
            .getReference("stickers-backup/${ref.name}/${ref.fileId}")
            .stream
        streamTask
            ?.addOnSuccessListener { snapshot ->
                snapshot.stream.use {
                    callback.onDataReady(it.readBytes())
                }
            }
            ?.addOnFailureListener {
                callback.onLoadFailed(it)
            }
    }

    override fun cleanup() {}

    override fun cancel() {
        MainApplication.cancelRequests(ref.fileId)
        streamTask?.cancel()
    }

    override fun getDataClass() = ByteArray::class.java

    override fun getDataSource() = DataSource.REMOTE
}