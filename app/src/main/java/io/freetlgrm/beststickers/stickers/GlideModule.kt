package io.freetlgrm.beststickers.stickers

import android.content.Context
import com.airbnb.lottie.LottieDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import io.freetlgrm.beststickers.models.Sticker

@GlideModule
class GlideModule : AppGlideModule() {

    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        registry.apply {
            prepend(ByteArray::class.java, LottieDrawable::class.java, TGLottieDecoder())
            append(Sticker::class.java, ByteArray::class.java, StickerLoader.Factory())
        }
    }
}