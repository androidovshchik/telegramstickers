package io.freetlgrm.beststickers.stickers

import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import io.freetlgrm.beststickers.MainApplication
import org.json.JSONObject

class JsonRequest(url: String, successListener: Response.Listener<JSONObject>, errorListener: Response.ErrorListener)
    : JsonObjectRequest(Method.GET, url, null, successListener, errorListener) {

    init {
        setShouldCache(false)
    }

    @Throws(AuthFailureError::class)
    override fun getHeaders(): Map<String, String> {
        return MainApplication.getHeaders()
    }
}
