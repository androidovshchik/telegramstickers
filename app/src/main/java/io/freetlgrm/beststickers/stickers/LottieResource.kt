package io.freetlgrm.beststickers.stickers

import android.animation.ValueAnimator
import com.airbnb.lottie.LottieDrawable
import com.bumptech.glide.load.engine.Initializable
import com.bumptech.glide.load.resource.drawable.DrawableResource

class LottieResource(drawable: LottieDrawable) : DrawableResource<LottieDrawable>(drawable), Initializable {

    override fun getResourceClass() = LottieDrawable::class.java

    override fun initialize() {
        drawable.apply {
            repeatCount = ValueAnimator.INFINITE
            playAnimation()
        }
    }

    override fun getSize() = 1

    override fun recycle() {
        drawable.apply {
            stop()
            removeAllAnimatorListeners()
            removeAllUpdateListeners()
            clearComposition()
        }
    }
}
